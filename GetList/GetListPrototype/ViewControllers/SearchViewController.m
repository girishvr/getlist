//
//  SearchViewController.m
//  GetList
//
//  Created by Nupur Mittal on 16/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "SearchViewController.h"
#import "CustomCollectionCell.h"

@interface SearchViewController ()<UISearchBarDelegate,UICollectionViewDataSource, UICollectionViewDelegate>{
    IBOutlet UISegmentedControl *segmentTPF;
    IBOutlet UISearchBar *mySearchBar;
    IBOutlet UICollectionView *collectionViewSearch;
    NSArray *arrayCollection;
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayCollection = @[];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)segmentValueChanged:(UISegmentedControl *)sender{
    
}

-(IBAction)searchWith:(NSString *)searchText{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = [NSString stringWithFormat:@"https://mygetlist.com/api/search/?q=%@&page_size=16&page=1",searchText];
    urlPath = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [activityIndicator startAnimating];
    [self clearCookies];
    [manager GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSLog([responseObject description]);
            [activityIndicator stopAnimating];
            arrayCollection = responseObject[@"search"];
            [collectionViewSearch reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        [activityIndicator stopAnimating];
    }];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Collection view delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrayCollection.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CustomCollectionCell *cell = (CustomCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCollectionCell" forIndexPath:indexPath];
    [cell.imageViewPlaces sd_setImageWithURL:[NSURL URLWithString:[arrayCollection objectAtIndex:indexPath.row][@"image_url"]]];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    int parts = 2;
    int padding = 15;
    return CGSizeMake(self.view.frame.size.width/parts-padding, self.view.frame.size.width/parts-padding);
}





#pragma mark - Search Bar Delegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
    return true;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [searchBar setShowsCancelButton:YES animated:YES];

}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text = @"";
    [searchBar resignFirstResponder];

}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = false;
    return true;
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = false;
    [searchBar resignFirstResponder];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [self searchWith:searchBar.text];
}

#pragma mark - 

-(void)clearCookies{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
}


@end
