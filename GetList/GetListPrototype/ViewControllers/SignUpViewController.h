//
//  SignUpViewController.h
//  GetListPrototype
//
//  Created by Nupur Mittal on 27/06/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignInDelegate <NSObject>
-(void)doneSignIn;
@end
@interface SignUpViewController : ViewController<UITextFieldDelegate, UIAlertViewDelegate, FBSDKLoginButtonDelegate>{

    IBOutlet UIButton *signUpButton;
    IBOutlet UIButton *buttonEmailLogin;
    IBOutlet UIView *viewLoggingIn;
    IBOutlet UIView *viewSigningUp;
    IBOutletCollection(UITextField) NSArray *collectionFields;
    IBOutlet UITextField *textFieldLoginEmail;
    IBOutlet UITextField *textFieldLoginPassword;
    IBOutlet FBSDKLoginButton *loginButton;
}
@property (nonatomic, strong)id <SignInDelegate> signInDelegate;
@property(nonatomic, strong)NSDictionary *userProfile;
@end
