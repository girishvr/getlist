//
//  NewsFeedViewController.m
//  GetList
//
//  Created by Nupur Mittal on 16/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "NewsFeedViewController.h"
#import "NewsFeedTableViewCell.h"
#import "DatesClass.h"

@interface NewsFeedViewController (){
    NSString *userId;
    NSDictionary *userDictionary;
    NSMutableArray *arrayUsers;
    NSMutableArray *arrayPins;
    NSMutableArray *tableData;
    IBOutlet UITableView *tableNewsFeed;
    BOOL shouldReload;
    BOOL isLoggedIn;
}

@end

@implementation NewsFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayPins = [NSMutableArray array];
    // Do any additional setup after loading the view.
    
    //https://mygetlist.com/api/users/davidfan/
    //https://mygetlist.com/api/users/davidfan/pins/?page_size=25
    //https://mygetlist.com/api/users/girish/flat/?page_size=15
    //https://d2unfupxcgpath.cloudfront.net/media/items/6.-Mariscos-Jalisco-e1400908482238.jpg
    shouldReload = false;

    [self checkAndLogin];

    
}
-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    shouldReload = false;    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (shouldReload) {
        [self checkAndLogin];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)checkAndLogin{
    isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"];
    if (isLoggedIn) {
        [self getUserDetails];
    }else{
        [self loadLoginPage];
    }
}

-(void)loadLoginPage{

    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignUpViewController *signUpViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [signUpViewController setUserProfile:nil];
    [signUpViewController setSignInDelegate:self];
//    [self addChildViewController:signUpViewController];
    [self presentViewController:signUpViewController animated:NO completion:nil];
    
}

#pragma login delegate

-(void)doneSignIn{
//    shouldReload = TRUE;
    if ([[[UserData userInfo] valueForKey:@"user"] valueForKey:@"username"]) {
        userId = [[[UserData userInfo] valueForKey:@"user"] valueForKey:@"username"];
        
        [self checkAndLogin];
    }else{
        //if unexpected the load dummy
        [self loadDummyDetails];
    }
    
}

-(void)loadDummyDetails{
    userId = @"davidfan";
    [self getUserDetails];

}

-(void)getUserDetails{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = [NSString stringWithFormat:@"https://mygetlist.com/api/users/%@/",userId];
    [activityIndicator startAnimating];
    [self clearCookies];
    [manager GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog([responseObject description]);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            userDictionary = [NSDictionary dictionaryWithDictionary:responseObject[@"user"]];
            userId = userDictionary[@"username"];
            [self getPinsForUser];
            [activityIndicator stopAnimating];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        [activityIndicator stopAnimating];

    }];

}

-(void)getPinsForUser{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = [NSString stringWithFormat:@"https://mygetlist.com/api/users/%@/flat/?page_size=20",userId];
    [activityIndicator startAnimating];
    [self clearCookies];
    [manager GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSLog([responseObject description]);
            [activityIndicator stopAnimating];
            arrayPins = responseObject[@"item"];
            arrayUsers = responseObject[@"user"];
            [self prepareArrayPins];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        [activityIndicator stopAnimating];
    }];
}


#pragma mark - tabel methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NewsFeedTableViewCell *cell = [[NewsFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"NewsFeedCell"];
    if (cell) {
        cell = (NewsFeedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"NewsFeedCell"];
    }
    
    NSString *imageUrlPathItem = [NSString stringWithFormat:@"https://d2unfupxcgpath.cloudfront.net/media/%@",tableData[indexPath.row][@"image"]];
    NSString *imageUrlPathProfile = [self getFBProfilePicUrl:[tableData[indexPath.row][@"user"] intValue]];
                                     
    [cell.imageViewItem sd_setImageWithURL:[NSURL URLWithString:imageUrlPathItem]];
    [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imageUrlPathProfile]];
    
    cell.labelFullName.text = [self getUserName:[tableData[indexPath.row][@"user"] intValue]];
    
    cell.labelHandle.text = [self getUserHandle:[tableData[indexPath.row][@"user"] intValue]];
    
    cell.labelMessage.text = tableData[indexPath.row][@"message"];
    cell.labelLikes.text = [NSString stringWithFormat:@"%d",[tableData[indexPath.row][@"like_count"] intValue]];
    cell.labelDateAdded.text = [DatesClass getTimeDifference:tableData[indexPath.row][@"created_at"]];
    return cell;
    
}

#pragma mark - Date methods

-(void)prepareArrayPins{
    shouldReload = false;
    tableData = [NSMutableArray array];
    for (NSDictionary *objects in arrayPins) {
        NSMutableDictionary *myObj = [NSMutableDictionary dictionaryWithDictionary:objects];
        myObj[@"dates"] = [DatesClass getDateFromString:objects[@"created_at"]];
        [tableData addObject:myObj];
    }
    NSSortDescriptor *sortDescp = [[NSSortDescriptor alloc] initWithKey:@"dates" ascending:NO];
    
    tableData = [[tableData sortedArrayUsingDescriptors:@[sortDescp]] mutableCopy];
    for (NSMutableDictionary *obj in tableData) {
        NSLog(@"%@",obj[@"dates"]);
    }
    [tableNewsFeed reloadData];

}


-(void)clearCookies{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
}

-(NSString *)getUserName:(int)userID{
    NSString *stringUName = @"";
    for (NSDictionary* items in arrayUsers) {
        if (userID == [items[@"id"] intValue]) {
            stringUName = [NSString stringWithFormat:@"%@ %@", items[@"first_name"], items[@"last_name"]];
            break;
        }
    }
    
    return stringUName;
}


-(NSString *)getUserHandle:(int)userID{
    NSString *stringUName = @"@";
    for (NSDictionary* items in arrayUsers) {
        if (userID == [items[@"id"] intValue]) {
            stringUName = [NSString stringWithFormat:@"@%@", items[@"username"]];
            break;
        }
    }
    
    return stringUName;
}
-(NSString *)getFBProfilePicUrl:(int)userID{
    NSString * picUrlPath =@"";
    for (NSDictionary* items in arrayUsers) {
        if (userID == [items[@"id"] intValue]) {
            picUrlPath = [NSString stringWithFormat:@"https://d2unfupxcgpath.cloudfront.net/media/%@", items[@"image"]];
            //graph.facebook.com/10102857853605294/pictures/9e0e45bc-aa56-445f-83cc-1959929bb116.jpg
            //graph.facebook.com/{{fid}}/picture?access_token={{access_token}}
            //https://d2unfupxcgpath.cloudfront.net/media/pictures/8a03742e-54a9-4a31-8857-02858c719f60.jpg
            break;
        }
    }

    return picUrlPath;
}
@end
