//
//  ViewController.m
//  GetListPrototype
//
//  Created by Nupur Mittal on 24/06/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "ViewController.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@interface ViewController (){
}
@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicator setTintColor:[UIColor blueColor]];
    [activityIndicator setHidesWhenStopped:YES];
    [activityIndicator stopAnimating];
    [activityIndicator setCenter:self.view.center];
    [self.view addSubview:activityIndicator];
    // Do any additional setup after loading the view.
    [self setUpNavBar];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:activityIndicator];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)setUpNavBar{
    
   // self.title = @"GetList";
    
    NSDictionary *attribute = @{
                                NSStrokeWidthAttributeName: @4.0,
                                NSStrokeColorAttributeName:[UIColor blueColor],
                                NSForegroundColorAttributeName:[UIColor whiteColor],
                                NSFontAttributeName:[UIFont systemFontOfSize:30.0 weight:20.0]
                                };//[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil];//
    
    [self.navigationController.navigationBar setTitleTextAttributes:attribute];
    
    [self.navigationController.navigationBar setTintColor:[UIColor blueColor]];
    
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 45 , 35)];
    [menuButton setImage:[UIImage imageNamed:@"menuWhite"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(showSideMenu) forControlEvents:UIControlEventTouchUpInside];
    menuButton.layer.borderWidth = 1.0;
    menuButton.layer.cornerRadius = 3.0;
    menuButton.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.5 alpha:0.2] CGColor];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setFrame:CGRectMake(0, 0, 36 , 36)];
    //    [addButton setImage:[UIImage imageNamed:@"plusRoundBlue"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [addButton setAlpha:0.5];
    [addButton setTitle:@"+" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addButton.titleLabel setFont:[UIFont boldSystemFontOfSize:30.0]];
    [addButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 4, 0)];
    
    addButton.layer.borderWidth = 2.0;
    addButton.layer.cornerRadius = 18.0;
    addButton.layer.borderColor = [[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5] CGColor];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];
    
}


-(void)showSideMenu{
    [[[UIAlertView alloc] initWithTitle:@"Disabled" message:@"GetList Prototype App" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    
}

-(void)goBack{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
