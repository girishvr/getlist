//
//  NewsFeedViewController.h
//  GetList
//
//  Created by Nupur Mittal on 16/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpViewController.h"

@interface NewsFeedViewController : ViewController<UITableViewDataSource, UITableViewDelegate, SignInDelegate>

@end
