//
//  LoginOptionViewController.m
//  GetList
//
//  Created by Nupur Mittal on 16/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "LoginOptionViewController.h"
#import <sys/sysctl.h>
@interface LoginOptionViewController (){
    NSMutableArray *arrayCollection;
    NSMutableArray *arrayProfiles;
    
    CGPoint scrollingPoint, endPoint;
    NSTimer *scrollingTimer;
    int positionScroll;
    float coeff;
    
    int parts;
    int padding;
}


@end

@implementation LoginOptionViewController

- (NSString *) platformType:(NSString *)platform
{
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    return platform;
}


- (NSString *) platform{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self beforeViewAppears];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    positionScroll = 0;
    [self setUpFaceBookLogin];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"]) {
        [self suspendSelf];
    }else{
        [self setUpNavBar];
        [self scrollSlowly];
        [self popUpAlertView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Views load

-(void)beforeViewAppears{
    parts = 2;
    padding = 10;
    
    
    NSString *platform = [self platform];
    if ([platform isEqualToString:@"iPhone4,1"] || [platform isEqualToString:@"iPhone5,1"] || [platform isEqualToString:@"iPhone5,2"] || [platform isEqualToString:@"iPhone5,3"] || [platform isEqualToString:@"iPhone5,4"]){
        
    }else{
        parts = 3;
        //        padding = 15;
    }
    
    
    coeff = 0.0;
    arrayCollection = [@[@"",@"",@"",@"",@"",@"",@"",@"",@"",@""] mutableCopy];
    arrayProfiles  = [NSMutableArray array];
    [self getImagesPath];

}


#pragma mark - FaceBook Function

-(void)setUpFaceBookLogin{
    loginButton = [[FBSDKLoginButton alloc] init];
    //    loginButton.center = viewAlertBox.center;
    loginButton.delegate = self;
    loginButton.readPermissions = @[@"public_profile", @"email"];
    //    [viewAlertBox addSubview:loginButton];
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    //    FBSDKLoginManager *manager = [[FBSDKLoginManager alloc] init];
    //    [manager logInWithReadPermissions:@[@"public_profile", @"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
    //        if (error) {
    //            // Process error
    //            NSLog(@"%@",error.description);
    //        } else if (result.isCancelled) {
    //            // Handle cancellations
    //            NSLog(@"Result Cancelled!");
    //        } else {
    //            // If you ask for multiple permissions at once, you
    //            // should check if specific permissions missing
    //
    //            if ([result.grantedPermissions containsObject:@"email"]) {
    //                // Do work
    //                [self getDetailsAndLogin];
    //
    //            }
    //        }
    //    }];
    
}

/*
 
 result:-
 
 {
 email = "girishr@anomaly.co.in";
 "first_name" = Testanomaly;
 gender = male;
 id = 109681036039016;
 "last_name" = Testanomlay;
 link = "https://www.facebook.com/app_scoped_user_id/109681036039016/";
 locale = "en_GB";
 name = "Testanomaly Testanomlay";
 timezone = "5.5";
 "updated_time" = "2015-06-19T10:12:52+0000";
 verified = 1;
 }
 
 
 */

-(IBAction)getDetailsAndLogin{
    
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 //NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[FBSDKAccessToken currentAccessToken] userID]];
                 [self continueLogIn:result];
             }
             else{
                 NSLog(@"%@",error.localizedDescription);
             }
         }];
    }
}

- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{
    NSLog([[FBSDKAccessToken currentAccessToken] userID]);
    
    if ([result.grantedPermissions containsObject:@"email"]) {
        // Do work
        [self getDetailsAndLogin];
        
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Email not received.\nPlease try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}

/*!
 @abstract Sent to the delegate when the button was used to logout.
 @param loginButton The button that was clicked.
 */
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
}

-(void)logUsingFBAccount{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = @"https://mygetlist.com/api/auth/connect/";
    
    [manager POST:urlPath parameters:@{@"access_token": [[FBSDKAccessToken currentAccessToken] tokenString]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[[UIAlertView alloc] initWithTitle:@"Success!" message:@"Account created. Start adding items to your list now.\n https://mygetlist.com" delegate:self  cancelButtonTitle:@"Later" otherButtonTitles: @"Ok",nil] show];
        [self suspendSelf];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        [self suspendSelf];

    }];
    
}
#pragma mark -Navigation
-(IBAction)alreadyHaveGetList{
    //load sign in
    
    if ([FBSDKAccessToken currentAccessToken]) {
        [self logUsingFBAccount];
    }else{
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SignUpViewController *signUpViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
        [signUpViewController setUserProfile:nil];
        
        [self presentViewController:signUpViewController animated:NO completion:nil];
        
    }
    
}

-(void)doneSignIn{
    [self suspendSelf];
}

-(IBAction)suspendSelf{
    if ([_loginDelegate respondsToSelector:@selector(doneLogIn)]) {
        [_loginDelegate doneLogIn];
    }
}

-(void)continueLogIn:(id)results{
//    [self suspendSelf];
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignUpViewController *signUpViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [signUpViewController setUserProfile:results];
    signUpViewController.signInDelegate = self;
    [self.navigationController pushViewController:signUpViewController animated:YES];
    
}

#pragma mark - User Function

-(void)getProfileImageResource{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = @"https://mygetlist.com/api/feature/users/";
    
    NSLog(urlPath);
    
    [manager GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog([responseObject description]);
        [arrayProfiles removeAllObjects];
        for (NSDictionary *objects in responseObject[@"user"]) {
            [arrayProfiles addObject:objects[@"image"]];
        }
        [self setUpProfileScroller];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }];
}

-(void)getImagesPath{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //22,38,39
    
    
    NSTimeInterval seconds = [[NSDate date] timeIntervalSince1970];
    
    int articleNumber = [@[@(22),@(38),@(39)][(int)seconds%3] intValue];
    
    NSString *urlPath = [NSString stringWithFormat:@"https://mygetlist.com/api/categories/%d/items/?quick=1&page_size=%d",articleNumber, parts*padding];
    
    NSLog(urlPath);
    
    [manager GET:urlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog([responseObject description]);
        [arrayCollection removeAllObjects];
        for (NSDictionary *objects in responseObject[@"item"]) {
            [arrayCollection addObject:objects[@"image_url"]];
        }
        [collectionViewBackGround reloadData];
        [self getProfileImageResource];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self getProfileImageResource];
        
    }];
    
}

-(void)setUpProfileImages:(int)indx{
    int padd = 20;
    
    if (indx == 0) {
        padd = 15;
    }else if (indx%3 == 0){
        padd = 30;
    }
    
    CGRect frame = CGRectMake(coeff + padd, 15, 60, 60);
    UIImageView *imageViewProfile = [[UIImageView alloc] initWithFrame:frame];
    imageViewProfile.layer.cornerRadius = 30.0;
    imageViewProfile.clipsToBounds = true;
    [imageViewProfile setBackgroundColor:[UIColor lightGrayColor]];
    //    [imageViewProfile setImage:[UIImage imageNamed:[NSString stringWithFormat:@"profile-%d",indx]]];
    NSString *imageUrlPath = [NSString stringWithFormat:@"https://d2unfupxcgpath.cloudfront.net/media/%@",arrayProfiles[indx]];
    [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:imageUrlPath]];
    [imageViewProfile setContentMode:UIViewContentModeScaleAspectFill];
    [scrollViewProfile addSubview:imageViewProfile];
    coeff = CGRectGetMaxX(imageViewProfile.frame);
    
    NSLog(@"Image - %d\n%@",indx,imageUrlPath);
}

-(void)setUpProfileScroller{
    [scrollViewProfile setContentSize:CGSizeMake(scrollViewProfile.frame.size.width * ceil(arrayProfiles.count/3.0), scrollViewProfile.frame.size.height)];
    for (int i=0; i<arrayProfiles.count; i++) {
        [self setUpProfileImages:i];
    }
    [self scrollRight];
}

-(void)setUpNavBar{
    
    self.title = @"GetList";
    
    NSDictionary *attribute = @{
                                NSStrokeWidthAttributeName: @4.0,
                                NSStrokeColorAttributeName:[UIColor blueColor],
                                NSForegroundColorAttributeName:[UIColor whiteColor],
                                NSFontAttributeName:[UIFont systemFontOfSize:30.0 weight:20.0]
                                };//[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil];//
    
    [self.navigationController.navigationBar setTitleTextAttributes:attribute];
    
    [self.navigationController.navigationBar setTintColor:[UIColor blueColor]];
    
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 45 , 35)];
    [menuButton setImage:[UIImage imageNamed:@"menuWhite"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(showSideMenu) forControlEvents:UIControlEventTouchUpInside];
    menuButton.layer.borderWidth = 1.0;
    menuButton.layer.cornerRadius = 3.0;
    menuButton.layer.borderColor = [[UIColor colorWithRed:0.0 green:0.0 blue:0.5 alpha:0.2] CGColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setFrame:CGRectMake(0, 0, 36 , 36)];
    //    [addButton setImage:[UIImage imageNamed:@"plusRoundBlue"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(showSideMenu) forControlEvents:UIControlEventTouchUpInside];
    [addButton setAlpha:0.5];
    [addButton setTitle:@"+" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addButton.titleLabel setFont:[UIFont boldSystemFontOfSize:30.0]];
    [addButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 4, 0)];
    
    addButton.layer.borderWidth = 2.0;
    addButton.layer.cornerRadius = 18.0;
    addButton.layer.borderColor = [[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5] CGColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];
    
}

#pragma mark - Collection view delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrayCollection.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CustomCollectionCell *cell = (CustomCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCollectionCell" forIndexPath:indexPath];
    [cell.imageViewPlaces sd_setImageWithURL:[NSURL URLWithString:[arrayCollection objectAtIndex:indexPath.row]]];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frame.size.width/parts-padding, self.view.frame.size.width/parts-padding);
}


#pragma mark - Animation

-(void)popUpAlertView{
    
    viewAlertBox.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.7 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        viewAlertBox.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // do something once the animation finishes, put it here
    }];
    
    //With the reverse animation with hiding the same View.
    
    //    yourView.transform = CGAffineTransformIdentity;
    //    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    //        yourView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    //    } completion:^(BOOL finished){
    //        yourView.hidden = YES;
    //    }];
}

- (void)scrollSlowly {
    endPoint = CGPointMake(0, 200);
    scrollingPoint = CGPointMake(0, 0);
    scrollingTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(scrollSlowlyToPoint) userInfo:nil repeats:YES];
}

- (void)scrollSlowlyToPoint {
    collectionViewBackGround.contentOffset = scrollingPoint;
    if (scrollingPoint.y>endPoint.y) {
        [scrollingTimer invalidate];
        [self scrollBackSlowly];
    }
    scrollingPoint = CGPointMake(scrollingPoint.x, scrollingPoint.y+0.2);
}

- (void)scrollBackSlowly {
    endPoint = CGPointMake(0, 0);
    scrollingPoint = collectionViewBackGround.contentOffset;
    scrollingTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(scrollSlowlyFromPoint) userInfo:nil repeats:YES];
}


- (void)scrollSlowlyFromPoint{
    collectionViewBackGround.contentOffset = scrollingPoint;
    if (scrollingPoint.y<endPoint.y) {
        [scrollingTimer invalidate];
        [self scrollSlowly];
    }
    scrollingPoint = CGPointMake(scrollingPoint.x, scrollingPoint.y-0.2);
}


#pragma mark -

#pragma mark Use a UIButton to scroll a UIScrollView Left or Right

-(IBAction)scrollRight{
    if((positionScroll+1)*3<arrayProfiles.count){
        positionScroll +=1;
        [scrollViewProfile scrollRectToVisible:CGRectMake(positionScroll*scrollViewProfile.frame.size.width, 0, scrollViewProfile.frame.size.width, scrollViewProfile.frame.size.height) animated:YES];
        NSLog(@"Position: %i",positionScroll);
    }
}
-(IBAction)scrollLeft{
    if(positionScroll>0){
        positionScroll -=1;
        [scrollViewProfile scrollRectToVisible:CGRectMake(positionScroll*scrollViewProfile.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height) animated:YES];
        NSLog(@"Position: %i",positionScroll);
    }
}

-(IBAction)showSideMenu{
    [[[UIAlertView alloc] initWithTitle:@"Disabled" message:@"GetList Prototype App" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
}

@end
