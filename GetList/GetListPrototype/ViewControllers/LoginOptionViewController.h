//
//  LoginOptionViewController.h
//  GetList
//
//  Created by Nupur Mittal on 16/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCollectionCell.h"
#import "SignUpViewController.h"


@protocol LoginDelegate <NSObject>

-(void)doneLogIn;

@end
@interface LoginOptionViewController : ViewController<UICollectionViewDataSource, UICollectionViewDelegate, FBSDKLoginButtonDelegate, SignInDelegate>{
    IBOutlet UICollectionView * collectionViewBackGround;
    IBOutlet UIScrollView *scrollViewProfile;
    IBOutlet UIButton *btnNext;
    IBOutlet UIButton *btnPrev;
    IBOutlet UIView *viewAlertBox;
    IBOutlet FBSDKLoginButton *loginButton;
}

@property (nonatomic, strong)id <LoginDelegate> loginDelegate;

@end
