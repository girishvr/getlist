//
//  SignUpViewController.m
//  GetListPrototype
//
//  Created by Nupur Mittal on 27/06/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController (){
    NSMutableDictionary *dictionaryRequest;
    BOOL isSigningUp;
}

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dictionaryRequest = [NSMutableDictionary dictionary];
    
    loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.delegate = self;
    loginButton.readPermissions = @[@"public_profile", @"email"];
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    if (isSigningUp) {
        [self fillUpFields];
        
        //init
        dictionaryRequest = [@{@"facebook_id":_userProfile[@"id"],
                               @"first_name":_userProfile[@"first_name"],
                               @"last_name":_userProfile[@"last_name"]} mutableCopy];

    }else{
//        loginButton = [[FBSDKLoginButton alloc] init];
//        //    loginButton.center = viewAlertBox.center;
//        loginButton.delegate = self;
////        loginButton.readPermissions = @[@"public_profile", @"email"];
//        //    [viewAlertBox addSubview:loginButton];
//        
//        [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
        
    }
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //@"/{user-id}/picture"
    if (isSigningUp) {
        [self getUserPicture];
    }else if ([FBSDKAccessToken currentAccessToken]){
        [self logUsingFBAccount];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUserProfile:(NSDictionary *)userProfile{
    if (userProfile) {
        isSigningUp = true;
        _userProfile = userProfile;
    }else{
        isSigningUp = false;
    }
    [viewLoggingIn setHidden:isSigningUp];
    [viewSigningUp setHidden:!isSigningUp];
}


#pragma mark - 

-(void)getUserPicture{
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"me?fields=picture.width(200).height(200)"
                                  parameters:nil
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
        //NSLog(result[@"picture" ][@"data"][@"url"]);
        NSDictionary *respDictionary = [NSDictionary dictionaryWithDictionary:(NSDictionary *)result];
        if ([[respDictionary allKeys] count]>0) {
            NSString *imgPath = respDictionary[@"picture" ][@"data"][@"url"];
            NSArray *arrayParts = [imgPath componentsSeparatedByString:@"/"];
            NSArray *arrayParts2 = [[arrayParts lastObject] componentsSeparatedByString:@"?"];
            //[dictionaryRequest setObject:arrayParts2.firstObject forKey:@"image"];
        }
    }];

}

-(void)fillUpFields{
    for (UITextField *field in collectionFields) {
        switch (field.tag) {
            case 0:{
                //first name
                field.text = _userProfile[@"first_name"];
            }
                break;
            case 1:{
                //lastname
                field.text = _userProfile[@"last_name"];
            }
                break;
            case 2:{
                //email
                field.text = _userProfile[@"email"];
            }
                break;
                
            default:
                break;
        }
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)signInComplete:(id)responseObject{
    [activityIndicator stopAnimating];
    NSDictionary *dictionaryAuth = [NSDictionary dictionaryWithDictionary:responseObject];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLoggedIn"];
    if (dictionaryAuth) {
        [UserData setUserInfo:dictionaryAuth];
//        [[NSUserDefaults standardUserDefaults] setValue:dictionaryAuth forKey:@"auth"];
        [self suspendSelf:nil];
        if ([_signInDelegate respondsToSelector:@selector(doneSignIn)]) {
            [_signInDelegate doneSignIn];
        }
    }
}

#pragma mark - IB Action
-(IBAction)loginUsingEmail:(id)sender{
    [self suspendAllKeyBoards];
    if (textFieldLoginEmail.text.length>0 && textFieldLoginPassword.text.length >0) {
        //[self getAuthenticateToken];
        [self logIntoAccount];
        //[self logUsingFBAccount];
    }
}

-(IBAction)suspendSelf:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)actionCreateMyAccount:(id)sender {
    for (UITextField *fields in collectionFields) {
        if (fields.text.length<1) {
            [[[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please fill all the fields." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            return;
        }
    }
    for (UITextField *fields in collectionFields) {

        switch (fields.tag) {
            case 0:
            {
                //first name
            }
                
                break;
            case 1:
            {
                //lastname
            }
                break;
            case 2:
            {
                //email
                [dictionaryRequest setObject:[fields.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"email"];

            }
                break;
            case 3:
            {
                //suggested user id
                [dictionaryRequest setObject:[fields.text stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"username"];
            }
                break;
            case 4:
            {
                //password
                [dictionaryRequest setObject:fields.text forKey:@"password"];
            }
                break;
            case 5:
            {
                //city
                [dictionaryRequest setObject:fields.text forKey:@"city"];
            }
                break;
                
            default:
                break;
        }
    }
    [self myGetListUserRegistration];
}



#pragma mark - API calls
-(void)logUsingFBAccount{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = @"https://mygetlist.com/api/auth/connect/";
    NSString *accsToken = [[FBSDKAccessToken currentAccessToken] tokenString];
    NSLog(accsToken);
    NSDictionary *param = @{@"access_token": accsToken};
    NSLog(param.description);
    [activityIndicator startAnimating];
    
    [self clearCookies];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    [manager POST:urlPath parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[[UIAlertView alloc] initWithTitle:@"Success!" message:@"User Logged In." delegate:nil  cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        [self signInComplete:responseObject];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        [activityIndicator stopAnimating];
    }];
    
    

    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
//    request.HTTPMethod = @"POST";
//    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:param options:0 error:NULL];
//    
//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSError* err;
//        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
//                                                             options:kNilOptions
//                                                               error:&err];
//        NSLog(@"%@", json);
//        NSLog(@"error - %@", error.localizedDescription);
//        [activityIndicator stopAnimating];
//    }] resume];
}


//-(void)logUsingFBAccount{
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSString *urlPath = @"https://mygetlist.com/api/auth/connect/";
//    
//    [manager POST:urlPath parameters:@{@"access_token": [[FBSDKAccessToken currentAccessToken] tokenString]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self signInComplete:responseObject];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
//    }];
//    
//}


//{"username": "admin", "password": "password"}

-(void)logIntoAccount{

    [self clearCookies];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = @"https://mygetlist.com/api/auth/login/";
    [activityIndicator startAnimating];
    [manager POST:urlPath parameters:@{@"username": [textFieldLoginEmail.text stringByReplacingOccurrencesOfString:@" " withString:@""], @"password": [textFieldLoginPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self signInComplete:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        [activityIndicator stopAnimating];
    }];
    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
//    request.HTTPMethod = @"POST";
//    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:@{@"username": [textFieldLoginEmail.text stringByReplacingOccurrencesOfString:@" " withString:@""], @"password": [textFieldLoginPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""]} options:0 error:NULL];
//    
//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSError* err;
//        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
//                                                             options:kNilOptions
//                                                               error:&err];
//        NSLog(@"%@", json);
//        NSLog(@"error - %@", error.localizedDescription);
//        [activityIndicator stopAnimating];
//    }] resume];

}



-(void)getAuthenticateToken{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = @"https://mygetlist.com/api/auth/token/";
    [activityIndicator startAnimating];
    
    [self clearCookies];

    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:urlPath parameters:@{@"username": [textFieldLoginEmail.text stringByReplacingOccurrencesOfString:@" " withString:@""], @"password": [textFieldLoginPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        [self logIntoAccount];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        [activityIndicator stopAnimating];
    }];

    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
//    request.HTTPMethod = @"POST";
//    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:@{@"username": [textFieldLoginEmail.text stringByReplacingOccurrencesOfString:@" " withString:@""], @"password": [textFieldLoginPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""]} options:0 error:NULL];
//    
//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSError* err;
//        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
//                                                             options:kNilOptions
//                                                               error:&err];
//        NSLog(@"%@", json);
//        NSLog(@"error - %@", error.localizedDescription);
//        [activityIndicator stopAnimating];
//
//    }] resume];

}

/*
 
 POST https://mygetlist.com/api/auth/register/
 
 application/json
 
 {"username": "thierry2", "email": "thierryschellenbach+thierry2@gmail.com", "password": "tester"}
 
 {
 
     "username": "",
     "first_name": "",
     "last_name": "",
     "facebook_id": null,
     "image": null,
     "cover": null,
     "url_website": null,
     "url_twitter": null,
     "url_gplus": null,
     "facebook_profile_url": "",
     "city": "",
     "country": "",
     "featured": false,
     "picture_count": 0,
     "about_me": "",
     "pin_count": 0,
     "add_count": 0,
     "like_count": 0,
     "comment_count": 0,
     "follower_count": 0,
     "following_count": 0,
     "visit_count": 0
 }
 
 */
-(void)myGetListUserRegistration{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *urlPath = @"https://mygetlist.com/api/auth/register/";
    [self clearCookies];
    [manager POST:urlPath parameters:dictionaryRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[[UIAlertView alloc] initWithTitle:@"Success!" message:@"Account created. Start adding items to your list now.\n https://mygetlist.com" delegate:self  cancelButtonTitle:@"Later" otherButtonTitles: @"Ok",nil] show];
        [self disableAllFields];
        [self signInComplete:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Please try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }];
}


#pragma mark -

-(void)disableAllFields{
    for (UITextField *fields in collectionFields) {
        [fields setUserInteractionEnabled:NO];
    }
    [signUpButton setUserInteractionEnabled:NO];
    
}

-(void)suspendAllKeyBoards{
    for (UITextField *fields in collectionFields) {
        [fields resignFirstResponder];
    }
    [textFieldLoginEmail resignFirstResponder];
    [textFieldLoginPassword resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self suspendAllKeyBoards];
    return YES;
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            //
            break;
            
        default:
            [self goToWebSite];
            break;
    }
}

-(void)goToWebSite{
    NSURL *url = [NSURL URLWithString:@"https://mygetlist.com"];
    [[UIApplication sharedApplication] openURL:url];
}


#pragma mark - FDSDK Login Button Delegate

- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{
    
    NSLog([[FBSDKAccessToken currentAccessToken] userID]);
    
    if ([FBSDKAccessToken currentAccessToken]) {
        // Do work
        [self logUsingFBAccount];
        
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Fail!" message:[NSString stringWithFormat:@"Could Not Login.\nPlease try again.\n%@", error.localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }

    
}


- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
}

-(void)clearCookies{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }

}

@end
