//
//  AppDelegate.m
//  GetListPrototype
//
//  Created by Nupur Mittal on 24/06/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>


//FACEBOOK_APP_ID = '273788246152792' //1110724258943152
//FACEBOOK_APP_SECRET = 'f5efc7b0d7f1c65b1d0d3fe93b1b88dd'
//user name - nothings
//pw - test123

@interface AppDelegate (){
    LoginOptionViewController *loginOptionVC;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"isLoggedIn"];

//    UIStoryboard* mainSB = (UIStoryboard*)[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UITabBarController* tabController = (UITabBarController*)[mainSB instantiateViewControllerWithIdentifier:@"tabBarController"];
//    [self.window setRootViewController:tabController];

    [self.window makeKeyAndVisible];
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

//-(void)showLoginOptions{
//    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    loginOptionVC = [mainSB instantiateViewControllerWithIdentifier:@"LoginOptionViewController"];
//    loginOptionVC.loginDelegate = self;
//    [self.window.rootViewController presentViewController:loginOptionVC animated:NO completion:nil];
//}
//-(void)doneLogIn{
//    if (loginOptionVC) {
//        [loginOptionVC dismissViewControllerAnimated:NO completion:nil];
//    }else{
//    }
//}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
