//
//  AppDelegate.h
//  GetListPrototype
//
//  Created by Nupur Mittal on 24/06/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginOptionViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,LoginDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

