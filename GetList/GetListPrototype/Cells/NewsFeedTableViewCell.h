//
//  NewsFeedTableViewCell.h
//  GetList
//
//  Created by Nupur Mittal on 16/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedTableViewCell : UITableViewCell{
    IBOutlet UIView *viewFeedCell;
}
@property(nonatomic, strong) IBOutlet UIImageView *imageViewProfile;
@property(nonatomic, strong) IBOutlet UIImageView *imageViewItem;
@property(nonatomic, strong) IBOutlet UILabel *labelFullName;
@property(nonatomic, strong) IBOutlet UILabel *labelHandle;
@property(nonatomic, strong) IBOutlet UILabel *labelMessage;
@property(nonatomic, strong) IBOutlet UIButton *btnAdd;
@property(nonatomic, strong) IBOutlet UILabel *labelDateAdded;
@property(nonatomic, strong) IBOutlet UILabel *labelLikes;
@property(nonatomic, strong) IBOutlet UITextField *textFieldComment;
@end
