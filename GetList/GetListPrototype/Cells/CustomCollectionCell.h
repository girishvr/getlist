//
//  CustomCollectionCell.h
//  GetListPrototype
//
//  Created by Nupur Mittal on 24/06/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionCell : UICollectionViewCell
{
    
}
@property(nonatomic, strong)IBOutlet UIImageView *imageViewPlaces;
@end
