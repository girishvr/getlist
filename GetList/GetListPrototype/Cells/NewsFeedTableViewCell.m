//
//  NewsFeedTableViewCell.m
//  GetList
//
//  Created by Nupur Mittal on 16/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "NewsFeedTableViewCell.h"

@implementation NewsFeedTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [viewFeedCell.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
