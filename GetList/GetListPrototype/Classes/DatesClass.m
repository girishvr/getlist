//
//  DatesClass.m
//  SocialSchools
//
//  Created by Girish Rathod on 14/05/14.
//  Copyright (c) 2014 Girish Rathod. All rights reserved.
//

#import "DatesClass.h"

@implementation DatesClass
#pragma mark - Date Processing Methods

+ (NSString *)getDurationStringFrom:(NSString *)fromDateStr toDate:(NSString *)toDateStr{
    NSString *durationString;
    
    NSDate *strdDate = [self getDateFromString:fromDateStr];
    NSDate *endDate = [self getDateFromString:toDateStr];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM hh:mm"];
    durationString = [NSString stringWithFormat:@"%@ - %@",[dateFormatter stringFromDate:strdDate],[dateFormatter stringFromDate:endDate]];
    
    return durationString;
}

+ (NSString *)getTimeStringFrom:(NSString *)fromDateStr toDate:(NSString *)toDateStr{
    NSString *durationString;
    
    NSDate *strdDate = [self getDateFromString:fromDateStr];
    NSDate *endDate = [self getDateFromString:toDateStr];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm"];
    durationString = [NSString stringWithFormat:@"%@ - %@",[dateFormatter stringFromDate:strdDate],[dateFormatter stringFromDate:endDate]];
    
    return durationString;
}



+ (NSString *)getEventsDateStringForPostFrom:(NSString *)dateString{
    NSDate *date = [self getDateFromString:dateString];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy, hh:mm"];
    return [NSString stringWithFormat:@"%@ - %@",[self getShortWeekDayFromDate:date],[dateFormatter stringFromDate:date]];
}

+ (NSDate *)getDateFromString:(NSString *)strDate{
    
    strDate = [strDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    strDate = [strDate stringByReplacingOccurrencesOfString:@"Z" withString:@""];
    
    NSArray *array =[strDate componentsSeparatedByString:@"."];
    strDate = [array objectAtIndex:0];

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setLocale:[NSLocale systemLocale]];
    NSDate *returnDate = [dateFormat dateFromString:strDate];
    return returnDate;
}

+ (NSString *)getOnlyDateFromDate:(NSDate *)dayDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd"];
    return [dateFormatter stringFromDate:dayDate];
}

+ (NSString *)getWeekDayFromDate:(NSDate *)dayDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"EEEE"];
    return [dateFormatter stringFromDate:dayDate];
}

+ (NSString *)getShortWeekDayFromDate:(NSDate *)dayDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"EE"];
    return [dateFormatter stringFromDate:dayDate];
}



+ (NSString *)getMonthAndYear:(NSDate *)eventDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM yyyy"];
    return [dateFormatter stringFromDate:eventDate];
    
}

+ (NSString *)getFullMonthNameFrom:(NSString *)dateString{
    NSDate *date = [self getDateFromString:dateString];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)getTimeDifference:(NSString *)strDate{
    strDate = [strDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    NSArray *array =[strDate componentsSeparatedByString:@"."];
    strDate = [array objectAtIndex:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:strDate];//this time is in UTC
    
    dateFromString = [self toLocalTime:dateFromString];
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:dateFromString];
    
    
    int numberOfHrs = secondsBetween / 3600;
    
    if (numberOfHrs<25){

        if (numberOfHrs < 1) {
            int minutes = secondsBetween / 60;
            if (minutes > 10){
                return [NSString stringWithFormat:@"%d %@",minutes,NSLocalizedString(@"label_minutes_ago", @"minutes ago")];
            }else{
                return NSLocalizedString(@"label_just_now", @"Just Now");
            }
        }else{
            return [NSString stringWithFormat:@"%d %@",numberOfHrs,NSLocalizedString(@"label_hours_ago", @"hours ago")];
        }
        
    }
    
    [dateFormatter setDateFormat:@"dd MMM HH:mm"];
    NSString *durationString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:dateFromString]];
    return durationString;

    
}


+ (NSDate *)toUTC:(NSDate *)date
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSInteger seconds = -[timeZone secondsFromGMTForDate: date];
    return [NSDate dateWithTimeInterval: seconds sinceDate: date];
}

+(NSDate *)toLocalTime:(NSDate *)date
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSInteger seconds = [timeZone secondsFromGMTForDate:date];
    return [NSDate dateWithTimeInterval:seconds sinceDate:date];
}

+(NSString *)getStringForInterval:(NSTimeInterval)interval{
    int numberOfHrs = interval / 3600;

    if (numberOfHrs<25)
        return [NSString stringWithFormat:@"%d %@",numberOfHrs,NSLocalizedString(@"label_hours_ago", @"hours ago")];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM HH:mm"];
//    NSString *durationString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:dateFromString]];
    return @"";//durationString;
}

+ (NSString *)getTimeStamp{
    //return epoch number
    return [NSString stringWithFormat:@"%lli", [@(floor([[NSDate date] timeIntervalSince1970] * 1000)) longLongValue]];
}

-(NSDate *) getDateFromStringWithTimezone:(NSString *)string{
    NSMutableString * correctedDateString = [[NSMutableString alloc] initWithString:string];
    [correctedDateString deleteCharactersInRange: NSMakeRange(22, 1)];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    return [formatter dateFromString:correctedDateString];
}


+(NSString *)getStringFromDateWithTimeZone:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    return [formatter stringFromDate:date];
}
@end
