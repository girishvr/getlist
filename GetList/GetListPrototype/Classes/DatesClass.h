//
//  DatesClass.h
//  SocialSchools
//
//  Created by Girish Rathod on 14/05/14.
//  Copyright (c) 2014 Girish Rathod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatesClass : NSObject

+ (NSString *)getDurationStringFrom:(NSString *)fromDateStr toDate:(NSString *)toDateStr;

+ (NSString *)getEventsDateStringForPostFrom:(NSString *)dateString;

+ (NSDate *)getDateFromString:(NSString *)strDate;

+ (NSString *)getOnlyDateFromDate:(NSDate *)dayDate;

+ (NSString *)getWeekDayFromDate:(NSDate *)dayDate;

+ (NSString *)getMonthAndYear:(NSDate *)eventDate;

+ (NSString *)getFullMonthNameFrom:(NSString *)dateString;

+ (NSString *)getTimeDifference:(NSString *)strDate;

+ (NSString *)getTimeStamp;

+ (NSString *)getShortWeekDayFromDate:(NSDate *)dayDate;

+ (NSString *)getTimeStringFrom:(NSString *)fromDateStr toDate:(NSString *)toDateStr;

+ (NSDate *)toUTC:(NSDate *)date;

+(NSDate *)toLocalTime:(NSDate *)date;

-(NSDate *)getDateFromStringWithTimezone:(NSString *)string;

+(NSString *)getDateFromStringWithTimezone:(NSDate *)date;

+(NSString *)getStringFromDateWithTimeZone:(NSDate *)date;
@end
