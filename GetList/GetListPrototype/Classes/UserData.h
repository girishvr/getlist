//
//  UserData.h
//  GetList
//
//  Created by Nupur Mittal on 30/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject
+(NSMutableDictionary *)userInfo;
+(void)setUserInfo:(id)rData;
@end
