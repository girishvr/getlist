//
//  UserData.m
//  GetList
//
//  Created by Nupur Mittal on 30/07/15.
//  Copyright (c) 2015 GVR. All rights reserved.
//

#import "UserData.h"
static NSMutableDictionary *userInfo = Nil;

@implementation UserData
+(NSMutableDictionary *)userInfo{
    return userInfo;
}

+(void)setUserInfo:(NSDictionary *)rData{
    if ([rData isKindOfClass:[NSDictionary class]]) {
        userInfo = [NSMutableDictionary dictionaryWithDictionary:rData];
    }
}
@end
